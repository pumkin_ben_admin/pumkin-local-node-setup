module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),


        watch: {
            sass: {
                files: [
                    'sass/*',
                    'sass/*/*'
                ],
                tasks: ['compass:dev']
            },

            watchFiles: {
                files: [
                    'js/max/*',
                    '*.php',
                    'partials/*',
                    'page_sections/*'
                ],
                options: {
                    livereload: true
                }
            },


            uglify: {
                files: [
                    'scripts/*'
                ],
                tasks: ['uglify:compile_scripts']
            },

            webfont: {
                files: [
                    'icons/*.svg'
                ],
                tasks: ['webfont']
            }
        },

        browserSync: {
            options: {
                watchTask: true,
            },
            files: {
                src: ['css/max/*.css', 'gfx/*']
            }
        },


        uglify: {

            plugins: {
                options: {
                    mangle: false,
                    beautify: false,
                    compress: true
                },
                files: {
                    'js/plugins.js': ['plugins/*.js']
                }
            },

            compile_scripts: {
                options: {
                    mangle: false,
                    beautify: true,
                    compress: false
                },
                files: {
                    'js/max/scripts.js': ['scripts/*.js']
                }  
            },

            minify_scripts: {
                files: {
                    'js/min/scripts.js': ['js/max/scripts.js']
                }
            }
        },


        compass: {
            dev: {
                options: {
                    sassDir: 'sass',
                    cssDir: 'css/max',
                    outputStyle: 'nested'
                }
            },
            dist: {
                options: {
                    sassDir: 'sass',
                    cssDir: 'css/min',
                    outputStyle: 'compressed'
                }
            }
        },

        webfont: {
          icons: {
            src: 'icons/*.svg',
            dest: 'Fonts/icons',
            destCss: 'sass/common',
            
            options: {
              stylesheet: 'scss',
              // template: 'icons/templates/_icon_template.scss',
              relativeFontPath: '../../Fonts/icons/',
              destHtml: 'Fonts/icons/preview',
              ligatures: true,
              templateOptions: {
                baseClass: 'icon',
                classPrefix: 'icon-',
                mixinPrefix: 'icon-'
              }
            }
          }
        }


    });

    // Load grunt plugins.
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-webfont');



    //register tasks


    //watch with css inject
    grunt.registerTask('default', ["browserSync", "watch"]);


    // compile all files that need compiling
    grunt.registerTask('c', ['compass', 'uglify']);

    // make icon font
    grunt.registerTask('icons', ['webfont']);

};