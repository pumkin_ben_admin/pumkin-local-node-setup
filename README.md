# Use this to build a site locally with Node.js

## Compiles less, coffee, and jade and refreshes page on changes with livereload
(see Grunfile.js for paths)

### Instruction for use

In terminal cd to folder.
Run: `$ npm install`

If you haven't installed the Grunt CLI - http://gruntjs.com/getting-started
	enter: `$ npm -g install grunt-cli)`

After all packages have installed, enter `$ grunt start`

This will initialise the watch on file changes and start the server at localhost:8000
