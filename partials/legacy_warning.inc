<aside id="legacyWarning">
	<p>Your browser is old and grouchy. Get a better one:</p>
	<ul>
		<li><a href="https://www.google.com/intl/en/chrome/browser/">chrome</a></li>
		<li><a href="http://support.apple.com/en_GB/downloads/#safari">safari</a></li>
		<li><a href="http://www.mozilla.org/en-US/firefox/new/">firefox</a></li>
	</ul>
</aside>