<?php 
if ($local) {
	echo "
	<script src='/js/max/scripts.js'></script>
	";
} else {
	echo "
	<script src='/js/min/scripts.js?v=".$time."'></script>
	";
}

if ($local) include "partials/browser_watch.inc";
?>
<div id="mobileTester"></div>
<div id="tabletTester"></div>
</body>
</html>