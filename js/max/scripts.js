var $body, $document, $window, HEIGHT, WIDTH, debouncedResize, isDevice, isMobile, isTablet, isTouch, onResize, throttledResize;

$window = $(window);

$body = $("body");

WIDTH = $window.width();

HEIGHT = $window.height();

isMobile = $("#mobileTester").css("visibility") === "visible" ? true : false;

isTablet = $("#tabletTester").css("visibility") === "visible" ? true : false;

isDevice = isMobile || isTablet ? true : false;

isTouch = Modernizr.touch ? true : false;

onResize = function() {
    WIDTH = $window.width();
    HEIGHT = $window.height();
};

throttledResize = $.throttle(250, function() {
    console.log("throttledResize");
});

debouncedResize = $.debounce(100, function() {
    console.log("debouncedResize");
});

$window.on("resize", onResize);

$window.on("resize", throttledResize);

$window.on("resize", debouncedResize);

$window.trigger("resize");